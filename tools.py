def print_board(board_list):
    """print out the game board with input board_list"""
    line = "+---+---+---+\n"
    output = line
    index = 0
    
    while index < 9:
        #current item:
        item =str(board_list[index])
        
        # 1st column : when index in 0, 3, 6  
        if index % 3 == 0:
            output = output + "| " + item
            
        # 2nd column : when index in 1, 4, 7
        elif index %3 == 1 :
            output = output + " | " + item
            
        # 3rd column : when index in 2, 5, 8
        elif index % 3 == 2:
            output = output +" | " + item + " |\n" + line
            
        index += 1
    print(output)


def game_over(board,current_player):
    """printout current board, current plyer won and exit the game"""
    print_board(board)
    print(current_player +' has won')
    exit()


def current_player_move(board,current_player):
    """enter playing board, current  return move_space"""
    response = int(input("Where would " + current_player + " like to move? "))
    while  type(board[response - 1]) != int:
        response = int(input("The space is not avabliable, please re-enter:"))
    board[response -1] = current_player


def swiched_player(current_player):
    """swich player and return"""
    if current_player == "X":
        return "O"
    else:
        return  "X"


def is_winning(board):
    # row win:
    if  (board[0] == board[1] and board[1] == board[2]) or (board[3] == board[4] and board[4] == board[5]) or (board[6] == board[7] and board[7] == board[8]):
        return True

    # column win
    if  (board[0] == board[3] and board[3] == board[6]) or (board[1] == board[4] and board[4] == board[7]) or (board[2] == board[5] and board[5] == board[8]):
        return True

    # diagonal win
    if (board[0] == board[4] and board[4] == board[8]) or (board[2] == board[4] and board[4] == board[6]):
        return True
    return False




