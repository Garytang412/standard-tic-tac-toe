from tools import print_board, game_over, current_player_move, swiched_player, is_winning

#setting board to a 9 space list
board = [1, 2, 3, 4, 5, 6, 7, 8, 9]

# setting current_player to 'X'
current_player = "X"

# main game:
for move_number in range(1, 10):
    #printout playing board:
    print_board(board)
    #asking for move:
    current_player_move(board,current_player)
    
    #check for winning: yes? -> game_over |  no? -> swich_plyer
    if is_winning(board):
        game_over(board,current_player)
    else:
        current_player = swiched_player(current_player)

# loop over, nowinner means a tie!
print_board(board)
print("It's a tie!")